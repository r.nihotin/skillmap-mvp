import { createStore, applyMiddleware, combineReducers } from 'redux';
import Router from 'next/router';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';
import { createWrapper } from 'next-redux-wrapper';
import {
  createRouterMiddleware,
  initialRouterState,
  routerReducer,
} from 'connected-next-router';
import {
  nextReduxCookieMiddleware,
  wrapMakeStore,
} from 'next-redux-cookie-wrapper';

const rootReducer = combineReducers({
  router: routerReducer,
});

export const makeStore = (context = {}) =>
  wrapMakeStore(() => {
    const { asPath } = context.ctx || Router.router || {};
    const routerMiddleware = createRouterMiddleware();

    const middlewares = [thunkMiddleware, routerMiddleware];

    let preloadedState = {
      router: initialRouterState(asPath),
    };

    // TODO: nextReduxCookieMiddleware: subtrees

    return createStore(
      rootReducer,
      preloadedState,
      composeWithDevTools(
        applyMiddleware(
          ...middlewares,
          nextReduxCookieMiddleware({
            subtrees: [],
          })
        )
      )
    );
  });

export default createWrapper(makeStore(), { debug: false });
