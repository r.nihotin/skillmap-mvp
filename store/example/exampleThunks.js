import {createAsyncThunk} from "@reduxjs/toolkit";
import {getData} from "../../services";

export const fetchData = createAsyncThunk('fetchData', async () => {
    const data = await getData('/example');
    return { data };
  }
);
