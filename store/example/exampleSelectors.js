import { createDraftSafeSelector } from '@reduxjs/toolkit';

const selectSelf = (state) => state.example;

export const getData = createDraftSafeSelector(selectSelf, (state) => state.data);
