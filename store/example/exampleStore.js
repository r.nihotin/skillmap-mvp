import { createSlice } from '@reduxjs/toolkit';

const getInitialState = () => ({
  data: [],
});

const storeName = 'shop';

const exampleStore = createSlice({
  name: storeName,
  initialState: getInitialState(),
  reducers: {
    addData: (store, { payload: { data } }) => {
      store.data = data;
    },
  },
  extraReducers: {
    [fetchData.fulfilled]: (store, { payload: { data } }) => {
      store.data = data;
    },
  },
});

export const {
  actions: {
    addData,
  },
} = exampleStore;

export { exampleStore };
