/**
 *
 * @param {string} title
 * @param {string} to
 * @return {{to: *, title: *}}
 */
export const createRoute = (title, to) => ({ title, to });
