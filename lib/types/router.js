import PropTypes from 'prop-types';

export const TRouter = PropTypes.shape({
  push: PropTypes.func,
});
