import * as yup from 'yup';
import {
  errorPhoneInvalid,
  errorRequired,
} from 'lib/constants';

import 'lib/validators/validationFunctions';

export const authFormSchema = yup.object().shape({
  phone: yup.string().ruPhone(errorPhoneInvalid).required(errorRequired),
});

