// text

// errors
export const errorRequired = 'Заполните поле';
export const errorEmailInvalid = 'Введите корректный email';
export const errorPhoneInvalid = 'Введите корректный номер телефона';
export const errorInnLength = 'ИНН должен содержать 10 или 12 цифр';