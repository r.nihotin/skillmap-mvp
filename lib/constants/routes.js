import { createRoute } from "../helpers";

const baseUrl = '';
const uploadsFolder = 'downloads';

export const termsOfUse = `${baseUrl}/${uploadsFolder}/terms-of-use.pdf`;

export const authRoute = createRoute('Аутентификация', '/auth');
export const catalogRoute = createRoute('Каталог', '/catalog');

export const headerRoutes = [catalogRoute];

export const mechanicaLink = 'https://mechanica.agency/';

export const socialLinks = {
  vk: 'https://vk.com/clubrusvinyl',
};
