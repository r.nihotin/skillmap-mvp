# Skillmaps Frontend Application

TODO: description

## Tech Stack

**Client:** React, Redux, SCSS, Next.js

**Server:** Node, Express, PostgreSQL


## Dev stand
### Application
http://***/
### API
http://***/
### Admin Panel
http://***/

## Production
### Application
https://***/
### API
https://***/
### Admin Panel
https://***/

## Run Locally

Add .env.local into app/ folder. Example is inside .env.example and values description is [here](#variables).  

### Install dependencies
#### Yarn
```bash
    yarn
```

#### NPM
```bash
    npm install
```

### Run project in dev mode
#### Yarn
```bash
    yarn dev
```

#### NPM
```bash
    npm run dev
```

### Run project in prod mode
#### Yarn
```bash
    yarn build && yarn start
```

#### NPM
```bash
    npm run build && npm run start
```

## <a name="variables"></a> Environment Variables For Developing
You can get the actual .env file from project manager.  

`NEXT_PUBLIC_BACKEND_URL` = `Path to api on site`  

`FRONTEND_PORT` = `Frontend port`  

## Authors

- [@r.nihotin](https://www.github.com/r.nihotin)
- [@i.ingovatov](https://gitlab.com/i.ingovatov)
- [@bogus077](https://gitlab.com/bogus077)

<img src="https://mechanica.agency/static/img/logo-black.svg" width="200"/>  

  
