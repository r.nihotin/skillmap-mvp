import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

class SkillmapsDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang="ru">
        <Head>
          {/* FAVICON */}
          <link rel="shortcut icon" href="/favicon.png" />

          {/* FONTS */}
          <link
            rel="preload"
            href="/fonts/SFUIDisplay-Regular.woff"
            as="font"
            type="font/woff"
            crossOrigin="anonymous"
          />
          <link
            rel="preload"
            href="/fonts/SFUIDisplay-Bold.woff"
            as="font"
            type="font/woff"
            crossOrigin="anonymous"
          />
          <link href="/fonts/style.css" rel="stylesheet" />
        </Head>

        <body>
        <Main />
        <NextScript />
        </body>
      </Html>
    );
  }
}

export default SkillmapsDocument;
