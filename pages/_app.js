import React from 'react';
import wrapper from '../store/store.js';
import { ConnectedRouter } from 'connected-next-router';
import '../asserts/scss/global.scss'

function MyApp({ Component, pageProps }) {
  return (
    <React.StrictMode>
      <ConnectedRouter>
        <Component {...pageProps} />
      </ConnectedRouter>
    </React.StrictMode>
  );
}

export default wrapper.withRedux(MyApp);
