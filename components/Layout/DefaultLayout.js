import React from 'react';
import Head from 'next/head';
import {defaultMeta, defaultPageTitle} from "../../lib/constants";
import styles from './DefaultLayout.module.scss';

function DefaultLayout(props) {
  const { children, title, meta } = props;
  return (
    <main className={styles.screen}>
      <Head>
        <title>{title || defaultPageTitle}</title>
        <meta name="description" content={meta || defaultMeta} key="description" />
      </Head>
      {children}
    </main>
  );
}

export default DefaultLayout;
